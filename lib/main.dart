import 'package:balanced_test/features/shop/presentation/page/shop_page.dart';
import 'package:flutter/material.dart';
import 'package:balanced_test/core/amplify_initializer.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final initAmplify = Amplifyinitializer();
  await initAmplify();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const ShopPage());
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:balanced_test/core/data_store_helper.dart';

class GlobalRepositoryProvider extends StatelessWidget {
  const GlobalRepositoryProvider({Key? key, required this.child})
      : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(providers: [
      RepositoryProvider(
        create: (context) => DataStoreHelper(),
      ),
    ], child: child);
  }
}

import 'package:amplify_api/amplify_api.dart';
import 'package:amplify_datastore/amplify_datastore.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:balanced_test/models/ModelProvider.dart';

import '../amplifyconfiguration.dart';

class Amplifyinitializer {
  const Amplifyinitializer._();

  static const _init = Amplifyinitializer._();

  factory Amplifyinitializer() {
    return _init;
  }

  Future<void> call() async {
    if (!Amplify.isConfigured) {
      await Amplify.addPlugins([
        AmplifyAPI(),
        AmplifyDataStore(modelProvider: ModelProvider.instance)
      ]);
      await Amplify.configure(amplifyconfig);
    }
  }
}

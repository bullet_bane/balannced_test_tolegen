import 'package:amplify_core/amplify_core.dart';
import 'package:amplify_flutter/amplify_flutter.dart';

class DataStoreHelper {
  const DataStoreHelper._();

  static const _iternal = DataStoreHelper._();

  factory DataStoreHelper() {
    return _iternal;
  }

  Future<void> save<T extends Model>(T model) async {
    return Amplify.DataStore.save(model);
  }

  Future<void> delete<T extends Model>(T model) async {
    return Amplify.DataStore.delete(model);
  }
}

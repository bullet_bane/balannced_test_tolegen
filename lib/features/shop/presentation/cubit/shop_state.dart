part of 'shop_cubit.dart';

@freezed
class ShopState with _$ShopState {
  const factory ShopState.initial() = _Initial;
  const factory ShopState.loadInProgress() = _LoadInProgress;
  const factory ShopState.loadFailed({required String error}) = _LoadFailed;
  const factory ShopState.loadSuccess({required ShopModel shop}) = _LoadSuccess;
}

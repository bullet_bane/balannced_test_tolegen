import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../data/model/ShopModel.dart';

part 'shop_state.dart';
part 'shop_cubit.freezed.dart';

class ShopCubit extends Cubit<ShopState> {
  ShopCubit() : super(const ShopState.initial());

  Future<void> fetchShop() async {
    emit(const ShopState.loadInProgress());
    try {
      final shop = await Amplify.DataStore.query<ShopModel>(
        ShopModel.classType,
        pagination: const QueryPagination.firstResult(),
      );

      emit(ShopState.loadSuccess(shop: shop.first));
    } catch (e) {
      emit(ShopState.loadFailed(error: e.toString()));
    }
  }
}

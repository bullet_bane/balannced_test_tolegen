import 'package:balanced_test/features/order/presentation/cubit/orders_cubit.dart';
import 'package:balanced_test/features/shop/presentation/cubit/shop_cubit.dart';
import 'package:balanced_test/features/shop/presentation/widgets/shop_body.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ShopPage extends StatelessWidget {
  const ShopPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => ShopCubit()..fetchShop(),
          ),
          BlocProvider(
            create: (context) => OrdersCubit(shopCubit: context.read()),
          ),
        ],
        child: BlocBuilder<ShopCubit, ShopState>(
          builder: (context, state) {
            return state.maybeWhen(
              loadInProgress: () {
                return const Center(
                  child: CircularProgressIndicator.adaptive(),
                );
              },
              loadFailed: (error) {
                return Center(
                  child: Text(error),
                );
              },
              loadSuccess: (shop) {
                return ShopBody(shop: shop);
              },
              orElse: () {
                return const SizedBox();
              },
            );
          },
        ),
      ),
    );
  }
}

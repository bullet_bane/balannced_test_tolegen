import 'package:balanced_test/features/order/presentation/cubit/orders_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'order_widget.dart';

class OrdersList extends StatelessWidget {
  const OrdersList({Key? key, required this.timeZone}) : super(key: key);

  final int timeZone;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrdersCubit, OrdersState>(
      builder: (context, state) {
        return state.maybeWhen(
            loadinProgress: () => const CircularProgressIndicator.adaptive(),
            loadSuccess: (orders) {
              return ListView.builder(
                  itemBuilder: (_, index) => OrderWidget(
                        order: orders[index],
                        timeZone: timeZone,
                      ));
            },
            orElse: () {
              return const SizedBox();
            });
      },
    );
  }
}

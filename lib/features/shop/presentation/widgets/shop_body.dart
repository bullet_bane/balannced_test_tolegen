import 'package:balanced_test/features/order/presentation/cubit/orders_cubit.dart';
import 'package:balanced_test/features/shop/presentation/widgets/orders_list.dart';
import 'package:balanced_test/models/ModelProvider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:table_calendar/table_calendar.dart';

class ShopBody extends StatelessWidget {
  const ShopBody({Key? key, required this.shop}) : super(key: key);
  final ShopModel shop;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TableCalendar(
          firstDay: DateTime.utc(2021, 1, 1),
          lastDay: DateTime.utc(2030, 3, 14),
          focusedDay: DateTime.now(),
          onDaySelected: (selectedDay, focusedDay) {
            context
                .read<OrdersCubit>()
                .fetchOrdersForThatDate(date: selectedDay);
          },
        ),
        OrdersList(timeZone: int.tryParse(shop.timeZone ?? '') ?? 0)
      ],
    );
  }
}

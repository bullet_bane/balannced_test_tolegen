import 'package:balanced_test/models/ModelProvider.dart';
import 'package:flutter/material.dart';

class OrderWidget extends StatelessWidget {
  const OrderWidget({Key? key, required this.order, required this.timeZone})
      : super(key: key);

  final OrderModel order;
  final int timeZone;

  @override
  Widget build(BuildContext context) {
    final time = order.start
        ?.getDateTimeInUtc()
        .add(Duration(hours: -timeZone))
        .add(DateTime.now().timeZoneOffset)
        .toString();

    if (time != null) {
      return Container(
        padding: const EdgeInsets.all(8.0),
        margin: const EdgeInsets.symmetric(
          horizontal: 17.0,
          vertical: 4.0,
        ),
        decoration: BoxDecoration(
            border: Border.all(
          color: Colors.black,
        )),
        child: Row(
          children: [
            Text(order.id),
            Text(time),
          ],
        ),
      );
    } else {
      return const SizedBox();
    }
  }
}

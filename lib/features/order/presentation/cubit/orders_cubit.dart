import 'package:balanced_test/features/shop/presentation/cubit/shop_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../data/model/OrderModel.dart';

part 'orders_state.dart';
part 'orders_cubit.freezed.dart';

class OrdersCubit extends Cubit<OrdersState> {
  OrdersCubit({required ShopCubit shopCubit})
      : _shopCubit = shopCubit,
        super(const OrdersState.initial());

  final ShopCubit _shopCubit;

  void fetchOrdersForThatDate({required DateTime date}) {
    emit(const OrdersState.loadInProgress());

    _shopCubit.state.whenOrNull(loadSuccess: (shop) {
      final orders = shop.orders;

      if (orders != null) {
        final ordersForDate = orders.where((order) {
          final orderStartDate = order.start?.getDateTimeInUtc();

          if (orderStartDate == null) {
            return false;
          }

          final orderStartDateWithoutTime = DateTime(
              orderStartDate.year, orderStartDate.month, orderStartDate.day);

          final selectedDateWithoutTime =
              DateTime(date.year, date.month, date.day);

          return orderStartDateWithoutTime
              .isAtSameMomentAs(selectedDateWithoutTime);
        }).toList();

        emit(OrdersState.loadSuccess(orders: ordersForDate));
      } else {
        emit(const OrdersState.loadFailed(error: "No orders for this date"));
      }
    });
  }
}

export type AmplifyDependentResourcesAttributes = {
    "api": {
        "FlutterTestBalanced": {
            "GraphQLAPIKeyOutput": "string",
            "GraphQLAPIIdOutput": "string",
            "GraphQLAPIEndpointOutput": "string"
        }
    }
}